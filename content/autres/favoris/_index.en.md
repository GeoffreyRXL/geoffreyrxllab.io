---
title: Favoris - web
description: Bookmarks
tags: ["favoris", "web"]
downloadBtn: true
# search keywords
keywords: ["favoris", "web"]

---

### News
- https://access.redhat.com/
- https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8
- https://github.com/containers/podman/blob/main/RELEASE_NOTES.md
- https://docs.openshift.com/
- https://www.digitalocean.com/community/tutorials?primary_filter=popular&hits_per_page=12
- https://linuxfr.org/
- https://slashdot.org/
- https://news.ycombinator.com/
- https://docs.ansible.com/ansible/latest/reference_appendices/release_and_maintenance.html
- https://hackernoon.com/
- https://www.theserverside.com/resources/DevOps-driven-cloud-native-app-development

### Bible
- https://www.brendangregg.com/linuxperf.html
- https://www.brendangregg.com/USEmethod/use-linux.html
- https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/pdf/security_hardening/red_hat_enterprise_linux-8-security_hardening-en-us.pdf

### Articles - Blogs techniques
- https://www.guru99.com/
- https://access.redhat.com/documentation/fr-fr/red_hat_enterprise_linux/8/pdf/managing_systems_using_the_rhel_8_web_console/red_hat_enterprise_linux-8-managing_systems_using_the_rhel_8_web_console-fr-fr.pdf
- https://linux.101hacks.com/toc/
- https://blog.christophersmart.com/2010/09/02/ssh-tricks/
- https://www.igvita.com/2009/06/23/measuring-optimizing-io-performance/
- https://www.rajeshvu.com/storage/emc/articles/srdf-operations
- https://www.flackbox.com/netapp-san-mpio-dsm-slm-host-utilities
- https://lhartikk.github.io/
- https://www.git-tower.com/learn/
- https://danielmiessler.com/
- https://www.commandlinefu.com/commands/browse
- https://www.geeksforgeeks.org/
- https://www.upwork.com/resources/soap-vs-rest-a-look-at-two-different-api-styles
- https://petermalmgren.com/signal-handling-docker/

### K8S
- https://media.defense.gov/2022/Aug/29/2003066362/-1/-1/0/CTR_KUBERNETES_HARDENING_GUIDANCE_1.2_20220829.PDF

### Repo
- https://github.com/iovisor/bcc
- https://git.floragunn.com/search-guard/search-guard-kibana-plugin
- https://github.com/hugapi/hug
- https://github.com/chassing/linux-sysadmin-interview-questions
- https://github.com/Netflix/bless
- https://github.com/m0rtem/CloudFail
- https://github.com/MiteshSharma/ImmutableInfrastructure
- https://github.com/jlevy/the-art-of-command-line/blob/master/README-fr.md
- https://github.com/krallin/tini
- https://github.com/bregman-arie/devops-exercises
- https://github.com/EbookFoundation/free-programming-books

### Pratique
- https://dateful.com/time-zone-converter
- https://www.text-image.com/convert/ascii.html
- https://www.lucidchart.com/pages/
- https://www.draw.io/

### AWS - Cloud
- https://awspolicygen.s3.amazonaws.com/policygen.html
- https://hackernoon.com/terraform-with-aws-assume-role-21567505ea98
- https://repost.aws/fr/knowledge-center
- https://blyx.com/2018/07/18/my-arsenal-of-aws-security-tools/
- https://github.com/zappa/Zappa
- https://github.com/duo-labs/cloudmapper
- https://github.com/prowler-cloud/prowler
- https://github.com/nccgroup/ScoutSuite
- https://arkadiyt.com/2019/11/12/detecting-manual-aws-console-actions/
- https://instances.vantage.sh/

### Autres
- https://seoi.net/penint/
- https://ellan-vannin.over-blog.com/
- https://sharp.dft.gov.uk/helmets/
- https://deaddrops.com/
- https://www.saveur-biere.com/fr/
- http://www.1001cocktails.com/
- https://libgen.li/
