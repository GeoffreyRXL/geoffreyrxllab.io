---
title: Bash tips
description: Le fourre-tout des astuces bash. Ici est regroupé des astuces plus ou moins connus que j'ai pu rencontrer.
tags: ["linux", "bash", "script"]
downloadBtn: true
# search keywords
keywords: ["linux", "bash", "script", "find", "yes", "del","tar","ssh","type","fdisk","root","zgrep","column"]

---

### Find
Chercher les fichiers modifiés sur les deux derniers jours en excluant aujourd'hui.
La liste des fichiers sera ouvert avec ```cat```
```bash
find / -mtime -2 -mtime +1 -exec ls -ld {} \; | cat
```

### Utiliser yes
```bash
yes | del *
  Remove file1?
  Remove file2?
```
Sinon :
```bash
yes n | del *
  Remove file1?
  Remove file2?
```

### Transférer un dossier avec tar et ssh
```bash
cd dossier
tar cf - . | ssh server2 'cd /tmp/dossier && tar xvf -'
```

### type -a
L'option ```-a``` permet d'afficher tous les emplacements contenant un exécutable du nom de votre commnande.
```bash
type -a python3
  python3 is /usr/bin/python3
  python3 is /bin/python3
```

### Automatiser fdisk
L'idée ici est de lui passer les commandes via echo. ```\n``` correspond à la touche Enter.
Par exemple créer une partition logique, pour du LVM:
```bash
echo -e "n\n\n\nt\n\n8e\nw\n" | fdisk /dev/sdb
```
On peut ainsi automatiser des actions sur plusieurs disques à la fois:
```bash
for i in $(cat /tmp/disks); do echo -e "n\n\n\nt\n\n8e\nw\n" | fdisk $i |  sudo format $i; done
```

### Shell incognito mode
La commande suivante vous permet de ne pas enregistrer l'historique de vos commandes de votre session shell bash actuelle:
```bash
unset HISTFILE && exit
```

### zgrep
Permet de chercher un pattern dans des dossiers compréssés:
```bash
zgrep eth0 /var/log/messages /var/log/messages.1.gz /var/log/messages.2.gz /var/log/messages.3.gz
```

### column
Pratique pour faire une sortie de commande bien ordonnée et plus lisible:

```bash
vmstat | column -t
r  b    swpd       free         buff    cache     si  so  bi  bo  in   cs  us  sy  id  wa  st
0  0    0          518463900    2108    373700    0   0   8   1   151  42  0   0   99  0   0 
```