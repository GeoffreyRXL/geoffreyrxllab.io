---
title: Magic keys
description: Magic Keys are ... magics
tags: ["linux"]
downloadBtn: true
# search keywords
keywords: ["linux", "magic", "keys", "sysrq"]

---

### Présentation
Après avoir exécuté la commande `sudo reboot` rien ne se passe. Après 5, 10, 15 minutes le serveur n'a toujours pas redémarré.

Trois solutions s'offrent à vous:
* Débrancher et rebrancher le serveur physiquement. *(Ceci ne devrait pas traverser votre esprit sain de sysadmin)*
* Se connecter à l'iLO ou DRAC pour redémarrer le serveur *(Pas pratique si on a dix serveurs à faire)*
* Utiliser les Magic Keys *(Un redémarrage propre, net, sans bavure en ligne de commande)*
 
<br><br>
 
Les Magic Keys peuvent être utilisées de deux manières:
- En utilisant le clavier avec une combinaison de touches.
- En écrivant une lettre qui correspond à une action dans ```/proc/sysrq-trigger```

{{< notice info >}}
**Documentation:** [https://en.wikipedia.org/wiki/Magic_SysRq_key](https://en.wikipedia.org/wiki/Magic_SysRq_key)
{{< /notice >}}

Les plus utilisés en cas de crise pour un reboot sans corruption de données sont ```s``` puis ```b```:

```bash
echo s | sudo tee /proc/sysrq-trigger
echo b | sudo tee /proc/sysrq-trigger
```
