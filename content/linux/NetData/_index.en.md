---
title: NetData
description: Présentation rapide de l'outil netdata
tags: ["linux", "monitoring"]
downloadBtn: true
# search keywords
keywords: ["linux", "monitoring", "netdata"]

---

### Présentation
Netdata est un outil de monitoring facile et pratique pour vos tests de performances.

### Installation
Netdata se trouve dans les repos:
```bash
sudo yum install netdata
```
### Utilisation
Une fois installé, vous pouvez le retrouver sous forme de service :
```bash
sudo systemctl status netdata.service
```

Les fichiers de configuration se trouve dans le dossier ```/etc/netdata/```.

Accéder à la console via votre navigateur web : ```http://<tonserveur>:19999 ```
![CPU](netdata_monitoring.jpg)

### Pour tester rapidement - à vos risques et périls

Faire chauffer un CPU:
```bash
yes > /dev/null
```

Read disk
```bash
hdparm -tv /dev/sdb
```

Write disk
```bash
dd if=/dev/zero of=/tmp/temp oflag=direct bs=128k count=16k
```
