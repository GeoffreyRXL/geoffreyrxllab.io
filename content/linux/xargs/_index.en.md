---
title: xargs -p
description: Parallélisation de commande.
tags: ["linux", "bash", "xargs"]
downloadBtn: true
# search keywords
keywords: ["linux", "bash", "xargs", "script"]

---

En bash, comment lancer plusieurs commandes en parralèle ? Réponse : ```xargs -P```

Avec deux options à comprendre :
* -I : input
* -P : max-procs : le nombre de processus que vous voulez lancer en parralèle.

### Exemple 1
Dans le script suivant nous allons :
- passer une suite de chiffre à xargs
- xargs lancera 3 commandes sleep en parallèle (-P 3) avec le chiffre de l'input
- Vous pouvez surveiller les processus avec la commande suivante : `watch "ps -aux | grep 'sleep' | grep -v 'grep sleep'"`

```bash
for i in 10 1 1 3 3 3; \
do \
  echo $i ;\
done | xargs -I{} -P3 bash -c 'sleep {} && echo "Cmd sleep {} is done"'
```

Résultat:
- Les 3 premières commandes sleep sont exécutées.
- Quand une commande finie, une autre est lancé. 

### Exemple 2
Reprenons le script précédent et ajoutons aux chiffres une autre variable:

```bash
for i in 10,dix 1,premier_un 1,deuxieme_un 3,premier_trois 3,deuxieme_trois 3,troisieme_trois; \
do \
echo $i ;\
done | xargs -I{} -P3 bash -c '\
  export values="{}" && \
  export number=$(echo $values | cut -d\, -f1) && \
  export string=$(echo $values | cut -d\, -f2) && \
  sleep $number && echo "Cmd sleep $string is done"'
```
